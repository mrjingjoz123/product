/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author 59161111
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "เอสเพรสโซ่ 1", 40, "1.jpg"));
        list.add(new Product(2, "เอสเพรสโซ่ 2", 50, "2.jpg"));
        list.add(new Product(3, "เอสเพรสโซ่ 3", 20, "3.jpg"));
        list.add(new Product(4, "นมปั่น 1", 20, "4.jpg"));
        list.add(new Product(5, "นมปั่น 2", 60, "5.jpg"));
        list.add(new Product(6, "นมปั่น 3", 50, "6.jpg"));
        list.add(new Product(7, "ชาขม 1", 40, "7.jpg"));
        list.add(new Product(8, "ชาขม 2", 40, "8.jpg"));
        list.add(new Product(9, "ชาขม 3", 70, "9.jpg"));
        list.add(new Product(10, "ชาขม 4", 20, "10.jpg"));
        return list;
    }
}
